<?php

/**
 * @file
 * Contains \Drupal\twig_testing\Controller\TwigTestingController.
 */

namespace Drupal\twig_testing\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for Twig testing routes.
 */
class TwigTestingController extends ControllerBase {

  /**
   * Displays a test page.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function nodePreview() {
    $node = node_load(1);
    if (!$node) {
      return array();
    }

    return array(
      '#theme' => 'node_preview',
      '#node' => $node,
    );
  }

}
